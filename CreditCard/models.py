from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from lib.basemodel import BaseModel


class CreditCard(BaseModel):
    card_number = models.CharField(verbose_name=_("credit_card"), max_length=16)

    def __str__(self):
        return self.card_number