from django.contrib import admin

# Register your models here.
from actions.models import Transaction, Transfer

admin.site.register(Transaction)
admin.site.register(Transfer)