from django import forms
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator, MinLengthValidator, \
    MaxLengthValidator, MinValueValidator, MaxValueValidator
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from actions.models import Transaction, Transfer

User = get_user_model()


class IncreaseForm(forms.Form):
    title = forms.CharField(max_length=32)
    amount = forms.IntegerField(
        required=True, max_value=1000000, min_value=1000,
        label=_("Enter how much you want to charge your account"),
        label_suffix='Enter amount'
    )

    def save(self, commit=False, user=None):
        if commit is True:
             try:
                 with transaction.atomic():
                     Transaction.objects.get_or_create(
                        title=self.cleaned_data['title'],
                        amount=self.cleaned_data['amount'],
                        transaction_type=Transaction.INCREASE,
                        user=user
                    )
             except:
                   raise forms.ValidationError(_("Some error found \n try again"))


class DecreaseForm(forms.Form):
    title = forms.CharField(max_length=32)
    amount = forms.IntegerField(
        required=True, max_value=1000000, min_value=1000,
        label=_("Enter how much you want to withdraw from your account"),
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)

    def clean(self):
        credit = User.objects.filter(pk=self.request.user.id).first().credit
        if self.cleaned_data['amount'] >= credit:
            raise forms.ValidationError(_(
                "Credit is not enough\n Enter amount less than {}".format(
                    credit)
            ))
        return self.cleaned_data

    def save(self, commit=False, user=None):
        if commit is True:
            try:
                with transaction.atomic():
                    Transaction.objects.get_or_create(
                        title=self.cleaned_data['title'],
                        amount=self.cleaned_data['amount'],
                        transaction_type=Transaction.DECREASE,
                        user=user
                    )
            except:
                raise forms.ValidationError(_("Some error found \n try again"))


class TransferForm(forms.Form):
    title = forms.CharField(max_length=32)
    phone_regex = RegexValidator(regex=r'(\+98|0)?9\d{9}')
    receiver = forms.CharField(validators=[phone_regex], required=True)
    amount = forms.IntegerField(
        required=True,
        label=_("Enter how much you want to withdraw from your account")
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)

    def clean(self):
        receiver = User.objects.filter(
            phone_number=self.cleaned_data['receiver']
        ).first()
        if receiver is None:
            raise forms.ValidationError(_(
                "user with provided phone number does not exist"
            ))
        user = User.objects.filter(pk=self.request.user.id).first()
        if self.cleaned_data['amount'] >= user.credit:
            raise forms.ValidationError(_(
                "Credit is not enough\n Enter amount less than {}".format(
                    user.credit)
            ))
        if user.phone_number == self.cleaned_data['receiver']:
            raise forms.ValidationError(_("You can not transfer money to yourself"))
        return self.cleaned_data

    def save(self, commit=False, user=None):
        if commit is True:
            receiver = User.objects.filter(phone_number=self.cleaned_data['receiver']).first()
            try:
                with transaction.atomic():
                    transaction_id, _ = Transaction.objects.get_or_create(
                        title=self.cleaned_data['title'],
                        amount=self.cleaned_data['amount'],
                        transaction_type=Transaction.TRANSFER,
                        user=user
                    )
                    Transfer.objects.get_or_create(
                        from_user=user,
                        to_user=receiver,
                        transaction=transaction_id
                    )
            except:
                raise forms.ValidationError(
                    _("Some error found \n try again"))