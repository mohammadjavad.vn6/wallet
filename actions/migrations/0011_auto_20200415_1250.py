# Generated by Django 2.2 on 2020-04-15 08:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('actions', '0010_remove_transaction_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='title',
            field=models.CharField(blank=True, max_length=32, null=True, verbose_name='title'),
        ),
    ]
