import datetime
import uuid

from django.db import models

from django.utils.translation import ugettext_lazy as _
# Create your models here.
from lib.basemodel import BaseModel


class Transaction(BaseModel):
    INCREASE = '0'
    DECREASE = '1'
    TRANSFER = '2'
    transaction_choices = (
        (INCREASE, "Increase"),
        (DECREASE, "Decrease"),
        (TRANSFER, "Transfer")
    )
    title = models.CharField(_("title"), max_length=32, null=True, blank=True)
    amount = models.IntegerField(_("amount"))
    transaction_type = models.CharField(_("transaction type"), max_length=50, choices=transaction_choices)
    UUID = models.UUIDField(verbose_name=_('uuid'), default=uuid.uuid4)
    user = models.ForeignKey("user.User", verbose_name=_(
        "user id"), on_delete=models.CASCADE, related_name="Transactions")


class Transfer(BaseModel):
    from_user = models.ForeignKey("user.User", verbose_name=_(
        "from user"), on_delete=models.CASCADE, related_name="from_user")
    to_user = models.ForeignKey("user.User", verbose_name=_(
        "to user"), on_delete=models.CASCADE, related_name="to_user")
    transaction = models.ForeignKey("actions.Transaction", verbose_name=_(
        "transaction id"), on_delete=models.CASCADE, related_name="Transfer_transaction")