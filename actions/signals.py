from django import forms
from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

from actions.models import Transaction, Transfer
from actions.tasks import increase_sms, decrease_sms, from_user_transfer, \
    to_user_transfer

User = get_user_model()


@receiver(post_save, sender=Transaction)
def modify_credit(sender, instance, created, *args, **kwargs):
    if created:
        if instance.transaction_type == Transaction.INCREASE:
            user = instance.user
            user.credit += instance.amount
            user.save()
            increase_sms.delay(
                 phone_number=user.phone_number, first_name=user.first_name,
                 last_name=user.last_name,
                 amount=instance.amount, credit=user.credit
             )
        if instance.transaction_type == Transaction.DECREASE:
            user = instance.user
            user.credit -= instance.amount
            user.save()
            decrease_sms.delay(
                user.phone_number, user.first_name, user.last_name,
                instance.amount, user.credit
            )


@receiver(post_save, sender=Transfer)
def modify_transfer_credit(sender, instance, created, *args, **kwargs):
    if created:
        from_user = instance.from_user
        to_user = instance.to_user
        from_user.credit -= instance.transaction.amount
        to_user.credit += instance.transaction.amount
        from_user.save()
        to_user.save()
        from_user_transfer.delay(
            from_user.phone_number, to_user.phone_number,
            from_user.first_name, from_user.last_name,
            instance.transaction.amount, from_user.credit
        )
        to_user_transfer.delay(
            from_user.phone_number, to_user.phone_number,
            from_user.first_name, from_user.last_name,
            instance.transaction.amount, from_user.credit
        )