from celery.task import task
from kavenegar import *
from OnlineWallet import local_settings
import requests


@task
def increase_sms(phone_number, first_name, last_name, amount, credit):
    url = 'https://api.kavenegar.com/v1/{}/sms/send.json'.format(local_settings.API_KEY)
    params = {
             'receptor': '{}'.format(phone_number),
             'message': 'Dear {} {} \n Your account charged with {} Rials'
                        '\n Your Credit: {}'.format(
                 first_name, last_name, amount, credit
             ),
              }
    response = requests.post(url=url, data=params)
    print(response)


@task
def decrease_sms(phone_number, first_name, last_name, amount, credit):
    url = 'https://api.kavenegar.com/v1/{}/sms/send.json'.format(local_settings.API_KEY)
    params = {
             'receptor': '{}'.format(phone_number),
             'message': 'Dear {} {} \n You withdraw {} Rials form your account'
                        '\n Your Credit: {}'.format(
                 first_name, last_name, amount, credit
             ),
              }
    response = requests.post(url=url, data=params)
    print(response)


@task
def from_user_transfer(
        phone_number_payer, phone_number_receiver, first_name, last_name,
        amount, credit
        ):
    url = 'https://api.kavenegar.com/v1/{}/sms/send.json'.format(
        local_settings.API_KEY)
    params = {
        'receptor': '{}'.format(phone_number_payer),
        'message': 'Dear {} {} \n You transfer {} Rials to {}'
                   '\n Your Credit: {}'.format(
            first_name, last_name, amount, phone_number_receiver, credit
        ),
    }
    response = requests.post(url=url, data=params)
    print(response)


@task
def to_user_transfer(
        phone_number_payer, phone_number_receiver, first_name, last_name,
        amount, credit
        ):
    url = 'https://api.kavenegar.com/v1/{}/sms/send.json'.format(
        local_settings.API_KEY)
    params = {
        'receptor': '{}'.format(phone_number_receiver),
        'message': 'Dear {} {} \n {} transfer {} Rials to your account'
                   '\n Your Credit: {}'.format(
            first_name, last_name, phone_number_payer, amount, credit
        ),
    }
    response = requests.post(url=url, data=params)
    print(response)