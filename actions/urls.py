from django.urls import path

from actions.views import IncreaseView, DecreaseView, TransferView

app_name = 'actions'
urlpatterns = [
    path('increase/', IncreaseView.as_view(), name='increase'),
    path('decrease/', DecreaseView.as_view(), name='decrease'),
    path('transfer/', TransferView.as_view(), name='transfer')
]