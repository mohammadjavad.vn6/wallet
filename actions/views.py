from django import forms
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _
from actions.tasks import increase_sms
# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import FormView

from actions.forms import IncreaseForm, DecreaseForm, TransferForm


class IncreaseView(LoginRequiredMixin, FormView):
    login_url = settings.LOGIN_URL
    template_name = "actions/increase.html"
    form_class = IncreaseForm
    success_url = reverse_lazy("account:profile")

    def form_valid(self, form):
        form.save(commit=True, user=self.request.user)
        return super().form_valid(form)


class DecreaseView(LoginRequiredMixin, FormView):
    login_url = settings.LOGIN_URL
    template_name = "actions/decrease.html"
    form_class = DecreaseForm
    success_url = reverse_lazy("account:profile")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def form_valid(self, form):
        form.save(commit=True, user=self.request.user)
        return super().form_valid(form)


class TransferView(LoginRequiredMixin, FormView):
    login_url = settings.LOGIN_URL
    template_name = "actions/transfer.html"
    form_class = TransferForm
    success_url = reverse_lazy("account:profile")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def form_valid(self, form):
         form.save(commit=True, user=self.request.user)
         return super().form_valid(form)