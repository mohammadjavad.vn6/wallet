

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class BaseModel(models.Model):
    created_time = models.DateTimeField(_("created time"), auto_now_add=True, null=True)
    modified_time = models.DateTimeField(_("Modified time"), auto_now=True)

    class Meta:
        abstract = True
