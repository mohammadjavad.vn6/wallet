from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

User = get_user_model()


class PhoneNumberBackend(ModelBackend):
    def authenticate(self, request, phone_number=None, **kwargs):
        try:
            user = User.objects.filter(phone_number=phone_number).first()
            return user
        except User.DoesNotExist:
            return None
