from django import forms
from django.contrib.auth import get_user_model, login
from django.utils.translation import ugettext_lazy as _
import datetime

from user.authentication import PhoneNumberBackend
from user.models import UserOTP

User = get_user_model()


class SignUpForm(forms.Form):
    username = forms.CharField()
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()
    phone_number = forms.CharField()
    password = forms.CharField()
    repeat_password = forms.CharField()

    def clean(self):
        user = User.objects.filter(
            username=self.cleaned_data["username"],
            phone_number=self.cleaned_data["phone_number"]).first()
        if user is not None:
            raise forms.ValidationError(_(
                "User with these provided data exist \n Try with another Phone Number")
            )
        if self.cleaned_data['password'] != self.cleaned_data["repeat_password"]:
            raise forms.ValidationError('Passwords are not equal together')
        return self.cleaned_data


class VerifyPhoneNumberForm(forms.Form):
    sms_code = forms.CharField(required=True, widget=forms.PasswordInput, label="Enter The Code")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.backend_authenticate = "user.authentication.PhoneNumberBackend"
        super().__init__(*args, **kwargs)

    def clean(self):
        if self.cleaned_data['sms_code'] is None:
            raise forms.ValidationError(_("This field is required"))

        user_code = UserOTP.objects.filter(
            code_type=UserOTP.SIGNUP,
            phone_number=self.request.session['phone_number']).last()
        user_code_expire_time = user_code.time_in_range(
            user_code.expire_time_start, user_code.expire_time_end,
            datetime.datetime.now()
        )
        if self.cleaned_data['sms_code'] != user_code.code:
            raise forms.ValidationError(_("This code is not True"))
        if user_code_expire_time is False:
            raise forms.ValidationError(_('This code is expired'))
        return self.cleaned_data

    def save(self, request):
        user = User.objects.create_user(
            username=request.session['data']['username'],
            first_name=request.session['data']['first_name'],
            last_name=request.session['data']['last_name'],
            email=request.session['data']['email'],
            phone_number=request.session['data']['phone_number'],
            password=request.session['data']['password']
        )
        user = PhoneNumberBackend.authenticate(
            self, request=self.request,
            phone_number=self.request.session["phone_number"]
        )
        login(self.request, user, backend=self.backend_authenticate)
        return user


class LoginForm(forms.Form):
    phone_number = forms.CharField(required=True)

    def clean(self):
        if self.cleaned_data['phone_number'] is None:
            raise forms.ValidationError(_("Phone number is required"))
        user = User.objects.filter(
            phone_number=self.cleaned_data['phone_number']).first()
        if user is None:
            raise forms.ValidationError(_
                ("User with this phone number does not exist")
            )
        return self.cleaned_data


class VerifySMSCodeForm(forms.Form):
    sms_code = forms.CharField(
        required=True, #widget=forms.PasswordInput,
        label="Enter the code which has sent"
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.backend_authenticate = "user.authentication.PhoneNumberBackend"
        super().__init__(*args, **kwargs)

    def clean(self):
        user_code = UserOTP.objects.filter(code_type=UserOTP.LOGIN,
            phone_number=self.request.session['phone_number']).last()

        user_code_expire_time = user_code.time_in_range(
            user_code.expire_time_start, user_code.expire_time_end,
            datetime.datetime.now()
        )
        if self.cleaned_data['sms_code'] != user_code.code:
            raise forms.ValidationError(_("This code is not True"))
        if user_code_expire_time is False:
            raise forms.ValidationError(_('This code is expired'))
        return self.cleaned_data

    def sign_in(self):
        user = PhoneNumberBackend.authenticate(
            self, request=self.request,
            phone_number=self.request.session["phone_number"]
        )
        login(self.request, user, backend=self.backend_authenticate)
        return user

