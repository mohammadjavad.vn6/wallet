import datetime

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractUser, PermissionsMixin, \
    UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
# Create your models here.
from django.utils import timezone

from CreditCard.models import CreditCard


class User(AbstractBaseUser, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_(
            'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    phone_regex = RegexValidator(
        regex=r'(\+98|0)?9\d{9}',
        message=_(
            "Phone number must be entered in the format: "
            "'+989999999999' or '09999999999'. Up to 11 digits allowed."
        ))
    phone_number = models.CharField(
        _("phone number"), validators=[phone_regex], max_length=17, unique=True
    )
    credit = models.BigIntegerField(_("credit"), default=0)
    credit_card = models.ForeignKey(
        CreditCard, verbose_name=_("credit card"), on_delete=models.CASCADE,
        related_name="card", null=True, blank=True
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)


class UserOTP(models.Model):
    SIGNUP = 1
    LOGIN = 2
    CODE_TYPE_CHOICES = (
        (SIGNUP, "sign_up"),
        (LOGIN, 'login')
    )
    code = models.CharField(verbose_name=_('code'), max_length=6)
    expire_time_start = models.DateTimeField(verbose_name=_("start of expire time"), default=datetime.datetime.now, null=True)
    expire_time_end = models.DateTimeField(_("end of expire time"), null=True)
    code_type = models.IntegerField(_("code type"), choices=CODE_TYPE_CHOICES, null=True)
    phone_number = models.CharField(_('phone number'), max_length=11, null=True)

    def time_in_range(self, start, end, x):
        """Return true if x is in the range [start, end]"""
        if start <= end:
            return start <= x <= end
        else:
            return start <= x or x <= end