from celery.task import task
from kavenegar import *
from OnlineWallet import local_settings
import requests


@task
def send_verification_sms(phone_number, code):
    url = 'https://api.kavenegar.com/v1/{}/sms/send.json'.format(local_settings.API_KEY)
    params = {
             'receptor': '{}'.format(phone_number),
             'message': 'Your verification code is: {}'.format(code),
              }
    response = requests.post(url=url, data=params)
    print(response)
    print("#" * 10, code, "*" * 10)