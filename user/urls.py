from django.urls import path

from user.views import SignUp, VerifyPhoneNumber, LogIn, VerifySMSCode, \
    Profile, LogoutAccount, RepeatLoginVerificationCode, \
    RepeatRegisterVerificationCode

app_name = 'account'
urlpatterns = [
    path('register/', SignUp.as_view(), name='sign_up'),
    path('verify-phone-number', VerifyPhoneNumber.as_view(), name='verify_phone_number'),
    path('login/', LogIn.as_view(), name='login'),
    path("verify-sms-code", VerifySMSCode.as_view(), name='verify_sms_code'),
    path("profile", Profile.as_view(), name="profile"),
    path("logout", LogoutAccount.as_view(), name="logout"),
    path("repeat-login-verification-code/", RepeatLoginVerificationCode.as_view(), name='login_otp'),
    path("repeat-register-verification-code/", RepeatRegisterVerificationCode.as_view(), name='register_otp')
]