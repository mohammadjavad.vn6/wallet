import datetime

from django import forms
from django.conf import settings
from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LogoutView
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy, reverse
from django.utils.crypto import get_random_string
from django.views.generic import FormView
from django.views.generic.base import View, TemplateView, RedirectView

from user.authentication import PhoneNumberBackend
from user.forms import SignUpForm, VerifyPhoneNumberForm, LoginForm, \
    VerifySMSCodeForm
from user.models import UserOTP
from user.tasks import send_verification_sms


User = get_user_model()


class SignUp(FormView):
    form_class = SignUpForm
    template_name = 'user/sign_up.html'

    def form_valid(self, form):
        phone_number = form.cleaned_data["phone_number"]
        code = get_random_string(length=6, allowed_chars="0123456789abcdef")
        OTP_code = UserOTP.objects.create(
            code=code, code_type=UserOTP.SIGNUP,
            expire_time_end=(datetime.datetime.now()+datetime.timedelta(minutes=1)),
            phone_number=phone_number
        )
        self.request.session['phone_number'] = phone_number
        self.request.session['code'] = code
        self.request.session['data'] = form.cleaned_data
        send_verification_sms.delay(phone_number, code)
        return HttpResponseRedirect(reverse('account:verify_phone_number'))


class VerifyPhoneNumber(FormView):
    form_class = VerifyPhoneNumberForm
    template_name = 'user/verify_signup_code.html'
    success_url = reverse_lazy('account:profile')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def form_valid(self, form):
        form.save(self.request)
        del self.request.session['data']
        del self.request.session['code']
        return super().form_valid(form)


class LogIn(FormView):
    form_class = LoginForm
    template_name = 'user/login.html'

    def form_valid(self, form):
        phone_number = form.cleaned_data["phone_number"]
        code = get_random_string(length=6, allowed_chars="0123456789abcdef")
        OTP_code = UserOTP.objects.create(
            code=code, code_type=UserOTP.LOGIN,
            expire_time_end=(datetime.datetime.now() + datetime.timedelta(
                minutes=1)),
            phone_number=phone_number
        )
        self.request.session["phone_number"] = phone_number
        self.request.session["sms_verify_code"] = code
        send_verification_sms.delay(phone_number, code)
        return HttpResponseRedirect(reverse('account:verify_sms_code'))


class VerifySMSCode(FormView):
    form_class = VerifySMSCodeForm
    template_name = 'user/verify_sms_code.html'
    success_url = reverse_lazy('account:profile')
    backend_authenticate = "user.authentication.PhoneNumberBackend"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def form_valid(self, form):
        form.sign_in()
        del self.request.session['sms_verify_code']
        del self.request.session["phone_number"]
        return super().form_valid(form)


class RepeatRegisterVerificationCode(RedirectView):
    url = reverse_lazy('account:verify_phone_number')

    def get(self, request, *args, **kwargs):

        code = get_random_string(length=6, allowed_chars="0123456789abcdef")
        OTP = UserOTP.objects.create(
            code=code,
            expire_time_end=(datetime.datetime.now() + datetime.timedelta(
                minutes=1)), code_type=UserOTP.SIGNUP,
            phone_number=request.session['phone_number']
        )
        send_verification_sms.delay(request.session['phone_number'], code)
        return super().get(request, *args, **kwargs)


class RepeatLoginVerificationCode(RedirectView):
    url = reverse_lazy('account:verify_sms_code')

    def get(self, request, *args, **kwargs):
        code = get_random_string(length=6, allowed_chars="0123456789abcdef")
        OTP = UserOTP.objects.create(
            code=code,
            expire_time_end=(datetime.datetime.now() + datetime.timedelta(
                minutes=1)), code_type=UserOTP.LOGIN,
            phone_number=request.session['phone_number']
        )
        send_verification_sms.delay(request.session['phone_number'], code)
        return super().get(request, *args, **kwargs)


class Profile(LoginRequiredMixin, View):
    login_url = settings.LOGIN_URL

    def get_object(self):
        user = User.objects.select_related("credit_card").prefetch_related("Transactions", "from_user").get(pk=self.request.user.id)
        return user

    def get(self, request):
        context = dict()
        user = self.get_object()
        transaction = user.Transactions.prefetch_related("Transfer_transaction")
        transfer = user.from_user.filter(from_user=user) | user.from_user.filter(from_user=user)
        #transfer = transaction.Transfer_transaction.filter(from_user=user)|transaction.Transfer_transaction.filter(to_user=user)
        context['first_name'] = user.first_name
        context['last_name'] = user.last_name
        context['credit'] = user.credit
        context['phone_number'] = user.phone_number
        context["credit_card"] = user.credit_card
        context["transaction"] = transaction
        context["transfer"] = transfer

        return render(request, 'user/profile.html', context=context)


class LogoutAccount(LogoutView):
    next_page = reverse_lazy('home')


class HomePage(TemplateView):
    template_name = "home.html"